Master branch status:
[![Build Status](https://travis-ci.org/TiBarification/Jail-Warden-Pro.svg?branch=master)](https://travis-ci.org/TiBarification/Jail-Warden-Pro)
[![CircleCI](https://circleci.com/gh/TiBarification/Jail-Warden-Pro/tree/master.svg?style=svg)](https://circleci.com/gh/TiBarification/Jail-Warden-Pro/tree/master)
Dev branch status:
[![Build Status](https://travis-ci.org/TiBarification/Jail-Warden-Pro.svg?branch=dev)](https://travis-ci.org/TiBarification/Jail-Warden-Pro)
[![CircleCI](https://circleci.com/gh/TiBarification/Jail-Warden-Pro/tree/dev.svg?style=svg)](https://circleci.com/gh/TiBarification/Jail-Warden-Pro/tree/dev)

[![GitHub issues](https://img.shields.io/github/issues/TiBarification/Jail-Warden-Pro.svg?style=flat-square)](https://github.com/TiBarification/Jail-Warden-Pro/issues)
### ENG: ###
This is Jail Warden Pro original Repositry
[Installation guide and FAQ for all of modules](http://tibarification.github.io/Jail-Warden-Pro/)
### РУС: ###
Это оригинальный репозиторий Jail Warden Pro
[Гайд по установке и FAQ по всем модулям](http://tibarification.github.io/Jail-Warden-Pro/)
